<?php

define('APP_DIR', __DIR__);
define('VIEW_DIR', __DIR__ . '/view');

include 'vendor/autoload.php';

function dd(...$params)
{
    echo '<pre>';
    var_dump($params);
    echo '</pre>';
    die();
}
