<?php

namespace App;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class EmotionApi
{
    private $url = 'https://westcentralus.api.cognitive.microsoft.com/face/v1.0/';
//    private $url = 'https://westus.api.cognitive.microsoft.com/emotion/v1.0/recognize';
    private $key;

    /**
     * EmotionApi constructor.
     * @param $key
     */
    public function __construct($key)
    {
        $this->key = $key;
    }


    public function send($params, UploadFile $file)
    {
        $result = [];

        try {

            $client = new Client([
                'base_uri' => $this->url,
            ]);

            $response = $client->post("detect?" . http_build_query($params), ['headers' => $this->getHeaders(), 'body' => $file->contents()]);

            $result = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

        } catch (RequestException $e) {
            throw new \Exception('Ошибка в процессе выполнения запроса: ' . $e->getMessage(), $e->getCode(), $e);
        }

        return $result;
    }

    private function getHeaders() {
        return [
//            'Content-Type' => 'application/json',
            'Content-Type' => 'application/octet-stream',
            'Ocp-Apim-Subscription-Key' => $this->key,
        ];
    }


}
