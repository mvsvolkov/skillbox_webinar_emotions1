<?php

namespace App;


class UploadFile
{
    private $file;

    private $maxSize = 4 * 1024 * 1024;
    private $types = ['image/jpeg', 'image/png', 'image/gif', 'image/bmp'];

    public function __construct($file)
    {
        $this->file = $file;
    }

    public function validate()
    {
        if (empty($this->file['tmp_name'])) {
            throw new \Exception('Не выбран файл');
        }

        if ($this->file['size'] > $this->maxSize) {
            throw new \Exception('Превышен размер файла');
        }

        if (! in_array($this->file['type'], $this->types)) {
            throw new \Exception('Можно загрузить только изображение');
        }

        return true;
    }

    public function getPath()
    {
        return $this->file['tmp_name'];
    }

    public function asBase64()
    {
        $type = pathinfo($this->getPath(), PATHINFO_EXTENSION);
        $data = file_get_contents($this->getPath());

        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }

    public function contents()
    {
        $data     = fopen($this->getPath(), 'rb');
        $contents = fread($data, filesize($this->getPath()));
        fclose($data);

        return $contents;
    }

}
