<?php
include __DIR__ . '/bootstrap.php';


if (! empty($_POST)) {

    try {
        $file = new \App\UploadFile($_FILES['file']);

        $file->validate();

        $parameters = [
            'returnFaceId'         => 'true',
            'returnFaceLandmarks'  => 'false',
//            'returnFaceAttributes' => 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
            'returnFaceAttributes' => 'age,gender,smile,glasses,emotion',
        ];
        $emotionApi = new \App\EmotionApi('Здесь ваш ключ');

        $results = $emotionApi->send($parameters, $file);

        $base64 = $file->asBase64();
    } catch (\Exception $e) {
        $error = $e->getMessage();
    }


}

function trans($key) {
    $langs = include 'langs.php';
    return $langs[$key] ?? $key;
}


include VIEW_DIR . '/header.php' ?>

<div class="py-5 text-center">
    Проект по распазнованию эмоций по фотографии. Поможет понять вам, стоит ли использовать фото на паспорт.
</div>

<div class="row">
    <div class="col-md-8">
        <h4>Распознавание эмоций</h4>
        <?php if (isset($error)) {?>
            <div class="alert alert-danger" role="alert">
                <?=$error?>
            </div>
        <?php } ?>
        <?php if (isset($base64)) {?>
            <div class="text-center mb-3">
                <img src="<?=$base64?>" style="max-width: 100%; max-height: 300px;">
            </div>
        <?php } ?>

        <form action="/" method="post" enctype="multipart/form-data">
            <input type="hidden" name="action" value="action">
            <div class="mb-3">
                <label>Выберите изображение</label>
                <input type="file" name="file" class="form-control">
            </div>
            <hr class="mb-3">
            <div class="mb-3">
                <button class="btn btn-primary">Анализировать</button>
            </div>
        </form>
    </div>

    <div class="col-md-4">
        <h4>Результаты</h4>
        <?php if (isset($results)) {?>
            <?php if (empty($results)) {?>
                <div class="alert alert-danger" role="alert">
                    Лица не найдены
                </div>
            <?php } ?>

            <?php foreach ($results as $faceId => $face) {?>
                <div class="alert alert-secondary" role="alert">
                    Лицо: <?=$faceId?>
                </div>
                <ul class="list-group mb-3">
                    <?php foreach ($face['faceAttributes'] as $key => $values) {?>
                        <li class="list-group-item d-flex justify-content-between">
                            <div>
                                <h6 class="my-0"><?=trans($key)?></h6>
                            </div>
                            <?php if (! is_array($values)) {?>
                                <span class="text-muted"><?=trans($values)?></span>
                            <?php } ?>


                            <?php if (is_array($values)) {?>
                                <ul class="list-group">
                                    <?php foreach ($values as $vKey => $value) {?>
                                        <li class="list-group-item d-flex justify-content-between">
                                            <small class="text-muted"><?=trans($vKey)?></small>

                                            <small class="text-muted" style="padding-left: 7px;"><?=trans($value)?></small>
                                        </li>
                                    <?}?>
                                </ul>
                            <?}?>
                        </li>
                    <?php } ?>
                </ul>
            <?php }?>

        <?php } else {?>
            <div>Загрузите фотографию...</div>
        <?php }?>
    </div>
</div>

<?php include VIEW_DIR . '/footer.php' ?>
