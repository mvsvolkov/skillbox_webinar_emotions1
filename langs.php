<?php

return [
    'smile'     => 'Улыбка',
    'emotion'   => 'Эмоции',
    'gender'    => 'Пол',
    'male'      => 'М',
    'female'    => 'Ж',
    'age'       => 'Возраст',
    'glasses'   => 'Очки',
    'NoGlasses' => 'Нет очков',

    'anger'     => 'Гнев',
    'contempt'  => 'Презрение',
    'disgust'   => 'Отвращение',
    'fear'      => 'Страх',
    'happiness' => 'Счастье',
    'neutral'   => 'Нейтрально',
    'sadness'   => 'Грусть',
    'surprise'  => 'Удивление',
];
